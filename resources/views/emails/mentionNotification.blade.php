<p>You were mentioned by {{ $author }}: <i>{{ $annotation->content }}</i></p>
<p><a href="{{ $link }}">Click here</a> to review it.</p>
