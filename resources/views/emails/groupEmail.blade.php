<p>Hello {{ $email->user->name }}, <br/><br/>
Here's what you missed on Hypothesis:
</p>

<br/>

@foreach($email->items as $item)
    <h4>{{  $item->title }}</h4>
    <p>{!!  $item->content !!}</p>
    <hr/>
@endforeach

<br/>
Kind regards,</br>
HypothesisApp


