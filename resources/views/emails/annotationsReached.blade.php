<p>As you are in the Hypothesis Group <b>{{ $group->name }}</b>, we just want to make you know there are <b>{{ $count }}</b> annotations available in the group.</p>
<p><a href="{{ $link }}">Click here</a> to review it.</p>

