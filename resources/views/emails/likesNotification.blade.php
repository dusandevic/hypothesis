<p>Your annotation has {{ $count }} likes: <i>{{ $annotation->content }}</i></p>
<p><a href="{{ $link }}">Click here</a> to review it.</p>
