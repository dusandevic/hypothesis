@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Groups</div>

                <div class="panel-body">   

                    <div class="row">
                        <form method="POST" action="/groups">
                            {{ csrf_field() }}
                            <div class="col-lg-3 col-sm-3">
                                <div class="input-group">
                                    <span class="input-group-addon">Name</span>
                                    <input type="text" class="form-control" name="name" required>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-3">
                                <div class="input-group">
                                    <span class="input-group-addon">H ID</span>
                                    <input type="text" class="form-control" name="hypothesis_id" required>
                                </div>
                            </div>
                            <div class="col-lg-4 col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">API Key</span>
                                    <input type="text" class="form-control" name="api_key" required>
                                </div>
                            </div>
                            <div class="col-lg-2 col-sm-2">
                                <button type="submit" class="pull-right btn btn-success btn-sm">Add</button>
                            </div>
                        </form>
                    </div>                 

                    <hr/>

                    <table style="width:100%">
                        <tr>
                            <th>Name</th>
                            <th>Hypothesis ID</th> 
                            <th>API Key</th>
                            <th>Actions</th>
                        </tr>
                        @foreach($groups as $group)
                        <tr>
                            <td>{{ $group->name }}</td>
                            <td>{{ $group->hypothesis_id }}</td> 
                            <td>{{ $group->api_key }}</td>
                            <td><a href="/delete-group/{!! $group->id !!}" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Remove</a></td>
                        </tr>
                        @endforeach
                    </table>                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
