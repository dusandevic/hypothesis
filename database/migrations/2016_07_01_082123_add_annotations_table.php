<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAnnotationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('annotations', function (Blueprint $table) {
            $table->increments("id");
            $table->string("hypothesis_id")->unique();
            $table->integer("group_id")->unsigned();
            $table->integer("user_id")->unsigned();
            $table->string("document", 255)->index(); //url
            $table->string("doc_title", 255)->index()->nullable(); //document title
            $table->text("content");
            $table->longText("source");
            $table->dateTime("published_at")->nullable();
            $table->timestamps();

            $table->foreign('group_id')
                    ->references('id')->on('groups')
                    ->onDelete('cascade');

            $table->foreign('user_id')
                    ->references('id')->on('users')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("annotations");
    }
}
