<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function annotations()
    {
        return $this->hasMany('App\Annotation');
    }

    public static function findByHypothesisUsername($username){
        $user = Self::where("hypothesis_username", $username)->take(1)->get();
        return isset($user[0]) ? $user[0] : false;
    }


    /*
     *  STATUS: RETIRED
     *  INFO: Can't use this approach as each user have his own link for tracking purposes
     */
    public static function notifyAll($emailTemplate, $dataObject){

        return false;

        /*

        foreach(Self::all() as $user){

            \Mail::send($emailTemplate, $data, function ($m) use ($userEmail, $userName, $data) {
                $m->from('noreply@hypo.dusandevic.com', 'Hypothesis App');
                $m->to($userEmail, $userName)->subject("New document annotated: " .$data['title']);
            });

        }*/
    }
}
