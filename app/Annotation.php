<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Annotation extends Model
{
    public function group()
    {
        return $this->belongsTo('App\Group');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function likes()
    {
        return $this->hasMany('App\Like', 'annotation_id', 'hypothesis_id');
    }

    public static function countByDocuments()
    {

        //select count(*) as annotations, a.`document`
        //from annotations a
        //group by a.`document`

        $report = \DB::table('annotations')->select(\DB::raw('count(*) as annotations, document'))
                        ->groupBy('document')
                        ->get();
        return $report;
    }

    public function scopeOfType($query, $days)
    {
        return $query->where('published_at', ">=", Carbon::now()->subDays($days));
    }
}
