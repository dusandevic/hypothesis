<?php


Route::get('/', "AnnotationsController@showHomepage");
Route::get('/link-router/{id}', "AnnotationsController@linkRedirect");

Route::get('/annotations', "AnnotationsController@getFeed");
Route::get('/test-rel', "AnnotationsController@testRelations");
Route::get('/test-doc', "AnnotationsController@annotationsByDocuments");
Route::get('/test-find', "AnnotationsController@testFind");
Route::get('/test-email', "AnnotationsController@testEmail");
Route::get('/test-mentions', "AnnotationsController@testMentions");
Route::get('/test-logs', "AnnotationsController@testLogs");
Route::get('/test-likes', "AnnotationsController@likesNotification");

Route::get('/test-juan', "AnnotationsController@testJuan");
Route::get('/test-new-doc', "AnnotationsController@testNewDoc");
Route::get('/send-emails', "AnnotationsController@sendEmails");

Route::get('likes', array('middleware' => 'cors', 'uses' => 'AnnotationsController@getLikes'));
Route::get('likes/{id}', array('middleware' => 'cors', 'uses' => 'AnnotationsController@getLikesForAnnotation'));
Route::delete('likes', array('middleware' => 'cors', 'uses' => 'AnnotationsController@deleteLikeForAnnotation'));
Route::post('/likes/', "AnnotationsController@postLike");

Route::get('logs', [
    'middleware' => 'auth',
    'uses' => '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index'
]);

Route::get('groups', 'AnnotationsController@getGroups')->middleware('auth');
Route::post('groups', 'AnnotationsController@postGroup')->middleware('auth');
Route::get('delete-group/{id}', 'AnnotationsController@deleteGroup')->middleware('auth');

Route::auth();

Route::get('/home', 'HomeController@index');
