<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mail extends Model
{
    public function items(){
        return $this->hasMany('App\MailItem');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
