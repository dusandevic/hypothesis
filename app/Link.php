<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    //

    public function getApplicationLink(){
        $link = env("APP_URL", "http://localhost") . "/link-router/".$this->id;
        return $link;
    }
}
