<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /*
         * E-mail sending implemented in ProcessAnnotations.php
         * */
         \App\Like::saved(function ($like) {
            if($like->user_id == env("JUAN_USERNAME", 'juan')){
                \Log::info("JUAN LIKES ANNOTATION ".$like->annotation_id."!");
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
