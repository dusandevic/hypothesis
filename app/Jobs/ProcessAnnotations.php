<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;


class ProcessAnnotations extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;


    public function __construct(){
        //\Log::info("ProcessAnnotations Constructor called!");
    }


    public function handle(){
        $this->juanLikedIt();
        $this->newDocumentNotification();
        $this->annotationsByDocuments();
        $this->mentionNotification();
        $this->likesNotification();
        $this->groupEmails();
        $this->sendEmails();
    }

    private function sendEmails(){
        $emails = \App\Mail::where('sent_at', null)->get();

        if($emails->count() == 0){
            \Log::info("No emails to send");
        }else{

            $sent = 0;

            foreach($emails as $email){
                $userEmail = $email->user->email;
                $userName = $email->user->name;
                $subject = $email->subject;
                $data['email'] = $email;

                \Mail::send('emails.groupEmail', $data, function ($m) use ($userEmail, $userName, $subject) {
                    $m->from('noreply@hypothesis.alperin.ca', 'Hypothesis App');
                    $m->to($userEmail, $userName)->subject($subject);
                });

                $email->touch();
                $email->save();

                $email->sent_at = $email->updated_at;
                $email->save();

                $sent++;
            }

            \Log::info($sent." emails are sent");
        }
    }

    private function groupEmails()
    {
        $mailItems = \App\MailItem::all()->groupBy('user_id');
        //dd($mailItems);

        $users = []; //list of the users that should be emailed

        foreach ($mailItems as $userItems) {
            $userId = $userItems[0]->user_id;
            $users[$userId] = false; //assumption: no new items to send


            foreach ($userItems as $emailItem) {
                is_null($emailItem->mail_id) ? $users[$userId] = true : ""; //if mail_id is null then it should be sent
            }

            if ($users[$userId]) {
                //there is something to send, create a new Email Instance;
                $mail = new \App\Mail();
                $mail->user_id = $userId;
                $mail->subject = "Hypothesis: New notifications for you";
                $mail->sent_at = null;
                $mail->save();

                //update mail_items table with our new mail_id;
                \App\MailItem::where('user_id', $userId)
                ->where('mail_id', null)
                ->update(['mail_id' => $mail->id]);
            }
        }
    }

    private function juanLikedIt(){
        $module_id = "JUAN_LIKES";
        $juanName = env("JUAN_USERNAME", "juan");


        $juanLikes = \App\Like::where("user_id", $juanName)->get();

        foreach($juanLikes as $annotation){
            \Log::info("Juan Alperin liked Annotation: ".$annotation->annotation_id.", we should notify it's author");
            $annot = \App\Annotation::where("hypothesis_id", $annotation->annotation_id)->first();

            if(is_null($annot)){
                \Log::warning("Juan Alperin liked Annotation: ".$annotation->annotation_id.", but there is no such annotation in our system");
            }else{
                $data['annotation'] = $annot;
                $data['title'] = $annot->doc_title;
                $userEmail = $annot->user->email;
                $userName = $annot->user->name;

                $alreadyNotified = $this->alreadyNotified($module_id, $annot->user->id, $annot->hypothesis_id);

                if(!$alreadyNotified){

                    $link = new \App\Link();
                    $link->endpoint     = "https://hypothes.is/a/".$annot->hypothesis_id;
                    $link->entity_id    = $annot->hypothesis_id;
                    $link->user_id      = $annot->user->id;
                    $link->clicked      = 0;
                    $link->module_name  = $module_id;
                    $link->save();

                    $data['link'] = $link->getApplicationLink();


                    $mailItem = new \App\MailItem();
                    $mailItem->user_id = $annot->user->id;
                    $mailItem->title = "Juan Alperin liked: " .$data['title'];
                    $mailItem->content = \View::make('emails.juanLikedIt', $data);
                    $mailItem->save();

                    /*
                        \Mail::send('emails.juanLikedIt', $data, function ($m) use ($userEmail, $userName, $data) {
                            $m->from('noreply@hypo.dusandevic.com', 'Hypothesis App');
                            $m->to($userEmail, $userName)->subject("Juan Alperin liked: " .$data['title']);
                        });
                    */

                    $log = new \App\MailLog();
                    $log->module_id = $module_id;
                    $log->user_id = $annot->user->id;
                    $log->entity_id = $annot->hypothesis_id;
                    $log->save();
                }else{
                    \Log::info("User ".$annot->user->name. " is already notified about module ".$module_id. " and annotation ".$annot->hypothesis_id);
                }
            }
        }
    }

    private function annotationsByDocuments(){
        $module_id = "DOCUMENTS";
        $documents = \App\Annotation::distinct('document')->select('document')->get();

        $documentsArray = [];
        $index = 0;

        //check documents and prepare the collection
        foreach($documents as $document){

           /*
            *  The notification for X many annotations on a page I've annotated should only happen if it has been
            *  at least 4 days since I annotated that page
            */

            $shouldNotify = false;


            $annotations = \App\Annotation::where("document", $document->document)->get();
            $documentsArray[$index]['url']              = $document->document;
            $documentsArray[$index]['title']            = $annotations[0]->doc_title;
            $documentsArray[$index]['count']            = $annotations->count();
            $documentsArray[$index]['annotations']      = $annotations;
            $documentsArray[$index]['shouldNotify']     = $annotations->count() >= 2 ? true : false;
            $index++;
        }


        foreach($documentsArray as $doc){
            if($doc['shouldNotify']){
                foreach($doc['annotations'] as $annotation){
                    $data['annotation'] = $annotation;
                    $data['count'] = $doc['count'];
                    $data['title'] = $doc['title'];
                    $data['url'] = $doc['url'];
                    $userEmail = $annotation->user->email;
                    $userName = $annotation->user->name;

                    $alreadyNotified = $this->alreadyNotified($module_id, $annotation->user->id, $doc['url']);

                    if(!$alreadyNotified){

                        $link = new \App\Link();
                        $link->endpoint     = $doc['url'];
                        $link->entity_id    = $annotation->hypothesis_id;
                        $link->user_id      = $annotation->user->id;
                        $link->clicked      = 0;
                        $link->module_name  = $module_id;
                        $link->save();

                        $data['emailLink'] = $link->getApplicationLink();

                        $mailItem = new \App\MailItem();
                        $mailItem->user_id = $annotation->user->id;
                        $mailItem->title = $data['title'].' reached ' .$data['count']. ' annotations!';
                        $mailItem->content = \View::make('emails.documentReached', $data);
                        $mailItem->save();

                        $log = new \App\MailLog();
                        $log->module_id = $module_id;
                        $log->user_id = $annotation->user->id;
                        $log->entity_id = $doc['url'];
                        $log->save();
                    }else{
                        \Log::info("User ".$annotation->user->name. " is already notified about module ".$module_id. " and entity (annotation) ".$annotation->hypothesis_id." in document: ". $doc['url']);
                    }
                }
            }
        }



    }

    //Function to check number of likes
    private function likesNotification(){
        $module_id = "COUNT_LIKES";
        $annotations = \App\Annotation::all();

        foreach($annotations as $annotation){
            $count = $annotation->likes->count();
            if($count >= 2){
                \Log::info("Annotation ".$annotation->hypothesis_id. " has ".$count . " annotations, we should notify author...");

                $alreadyNotified = $this->alreadyNotified($module_id, $annotation->user->id, $annotation->hypothesis_id);

                if(!$alreadyNotified){
                    $userEmail = $annotation->user->email;
                    $userName = $annotation->user->name;

                    $link = new \App\Link();
                    $link->endpoint     = "https://hypothes.is/stream?q=user:".$annotation->user->hypothesis_username;
                    $link->entity_id    = $annotation->hypothesis_id;
                    $link->user_id      = $annotation->user->id;
                    $link->clicked      = 0;
                    $link->module_name  = $module_id;
                    $link->save();

                    $emailLink = $link->getApplicationLink();

                    $data = [   "count" => $count,
                                "annotation" => $annotation,
                                "link" => $emailLink
                    ];

                    $mailItem = new \App\MailItem();
                    $mailItem->user_id = $annotation->user->id;
                    $mailItem->title = 'Your annotation has '.$data['count'].'  likes!';
                    $mailItem->content = \View::make('emails.likesNotification', $data);
                    $mailItem->save();

                    /*
                        \Mail::send('emails.likesNotification', $data, function ($m) use ($userEmail, $userName, $data) {
                            $m->from('noreply@hypo.dusandevic.com', 'Hypothesis App');
                            $m->to($userEmail, $userName)->subject('Your annotation has '.$data['count'].'  likes!');
                        });
                    */

                    $log = new \App\MailLog();
                    $log->module_id = $module_id;
                    $log->user_id = $annotation->user->id;
                    $log->entity_id = $annotation->hypothesis_id;
                    $log->save();
                }else{
                    \Log::info("User ".$annotation->user->name. " is already notified about module ".$module_id. " and entity (annotation likes) ".$annotation->hypothesis_id);
                }
            }
        }
    }

    //Function to check and send notifications to mentioned users
    private function mentionNotification(){
        $module_id = "MENTION";
        $annotations = \App\Annotation::all();

        $mentions = [];
        foreach($annotations as $annot){
            $hasMention = $this->checkMentions($annot->content);
            if($hasMention){
                $mentions[$annot->id] = $hasMention;
            }

        }

        foreach($mentions as $annotID => $userArray){
            $annotation = \App\Annotation::findOrFail($annotID);

            foreach($userArray as $mention){
                $mention = substr($mention, 1); //to cut @
                $mentionedUser = \App\User::findByHypothesisUsername($mention);

                if($mentionedUser){

                    $data['user'] = $mentionedUser;
                    $data['author'] = $annotation->user->name . " (@" .$annotation->user->hypothesis_username.")";
                    $data['annotation'] = $annotation;
                    $userEmail = $mentionedUser->email;
                    $userName = $mentionedUser->name;

                    $alreadyNotified = $this->alreadyNotified($module_id, $mentionedUser->id, $annotation->hypothesis_id);

                    if(!$alreadyNotified){

                        $link = new \App\Link();
                        $link->endpoint     = "https://hypothes.is/stream?q=user:".$mention;
                        $link->entity_id    = $annotation->hypothesis_id;
                        $link->user_id      = $mentionedUser->id;
                        $link->clicked      = 0;
                        $link->module_name  = $module_id;
                        $link->save();

                        $emailLink = $link->getApplicationLink();
                        $data['link'] = $emailLink;

                        $mailItem = new \App\MailItem();
                        $mailItem->user_id = $mentionedUser->id;
                        $mailItem->title = 'New mention by '.$data['author'].'!';
                        $mailItem->content = \View::make('emails.mentionNotification', $data);
                        $mailItem->save();

                        /*
                            \Mail::send('emails.mentionNotification', $data, function ($m) use ($userEmail, $userName, $data) {
                                $m->from('noreply@hypo.dusandevic.com', 'Hypothesis App');
                                $m->to($userEmail, $userName)->subject('New mention by '.$data['author'].'!');
                            });
                        */

                        $log = new \App\MailLog();
                        $log->module_id = $module_id;
                        $log->user_id = $mentionedUser->id;
                        $log->entity_id = $annotation->hypothesis_id;
                        $log->save();
                    }else{
                        \Log::info("User ".$mentionedUser->name. " is already notified about module ".$module_id. " and entity (annotation) ".$annotation->hypothesis_id);
                    }

                }else{
                    \Log::warning("User @".$mention. " was mentioned in annotation #".$annotID." but that user profile is missing");
                }
            }
        }
    }

    //Function to parse provided string and match all @mentions
    private function checkMentions($string){
        $matches = null;
        $returnValue = preg_match_all('/\\B@[a-z0-9_-]+/', $string, $matches);
        return isset($matches[0]) ? $matches[0] : false;
    }

    //Function to count number of annotations by groups
    //Status: RETIRED
    private function notificationCountCheck(){
        $module_id = "COUNT_ANNOTATIONS_BY_GROUPS";
        $groups = \App\Group::all();

        foreach($groups as $group){
            $count = $group->annotations->count();
            if($count > 3){
                \Log::info("Group ".$group->name. " has ".$count . " annotations, we should notify our users...");

                $usersToNotify = [];
                foreach($group->annotations as $annotation){


                    $data = [   "count" => $count,
                        "group" => $group,
                        "user" => $annotation->user,
                    ];
                    $usersToNotify[$annotation->user->id] = $annotation->user; //to avoid many emails to the same user
                }

                foreach($usersToNotify as $user){

                    $alreadyNotified = $this->alreadyNotified($module_id, $user->id, $group->hypothesis_id);

                    if(!$alreadyNotified){

                        $link = new \App\Link();
                        $link->endpoint     = "https://hypothes.is/stream?q=group:".$group->hypothesis_id;
                        $link->entity_id    = $group->hypothesis_id;
                        $link->user_id      = $user->id;
                        $link->clicked      = 0;
                        $link->module_name  = $module_id;
                        $link->save();

                        $emailLink = $link->getApplicationLink();
                        $data['link'] = $emailLink;

                        $userEmail = $user->email;
                        $userName = $user->name;


                        $mailItem = new \App\MailItem();
                        $mailItem->user_id = $user->id;
                        $mailItem->title = 'Group has '.$data['count'].'  annotations!';
                        $mailItem->content = \View::make('emails.annotationsReached', $data);
                        $mailItem->save();

                        /*
                        \Mail::send('emails.annotationsReached', $data, function ($m) use ($userEmail, $userName, $data) {
                            $m->from('noreply@hypo.dusandevic.com', 'Hypothesis App');
                            $m->to($userEmail, $userName)->subject('Group has '.$data['count'].'  annotations!');
                        });
                        */

                        $log = new \App\MailLog();
                        $log->module_id = $module_id;
                        $log->user_id = $user->id;
                        $log->entity_id = $group->hypothesis_id;
                        $log->save();
                    }else{
                        \Log::info("User ".$user->name. " is already notified about module ".$module_id. " and entity (group) ".$group->hypothesis_id);
                    }


                }
            }else{
                \Log::info("Group ".$group->name. " does not have enough annotations, no further action needed.");
            }
        }
    }

    private function alreadyNotified($module_id, $user_id, $entity_id){
        $notified = \App\MailLog::where("module_id", $module_id)
        ->where("user_id", $user_id)
        ->where("entity_id", $entity_id)
        ->count();

        return $notified > 0 ? true : false;
    }

    public function cutUsernameString($string){
        $cutPrefix = substr($string, 5);
        $cutSufix = str_replace("@hypothes.is", "", $cutPrefix);
        return $cutSufix;
    }

    private function newDocumentNotification(){
        $module_id = "NEW_DOCUMENT_ANNOTATED";
        $documents = \App\Annotation::countByDocuments();

        foreach($documents as $document){
            if($document->annotations == 1){
                \Log::info("New document detected: ".$document->document);

                foreach(\App\User::all() as $user){

                    $alreadyNotified = $this->alreadyNotified($module_id, $user->id, $document->document);

                    if(!$alreadyNotified){
                        $link = new \App\Link();
                        $link->endpoint     = $document->document;
                        $link->entity_id    = $document->document;
                        $link->user_id      = $user->id;
                        $link->clicked      = 0;
                        $link->module_name  = $module_id;
                        $link->save();

                        $email = $user->email;
                        $name = $user->name;

                        $data = [];
                        $data['topic']      = $module_id;
                        $data['subject']    = "New Document annotated: ".$document->document;
                        $data['link']       = $link->getApplicationLink();
                        $data['user']       = $user;

                        $mailItem = new \App\MailItem();
                        $mailItem->user_id = $user->id;
                        $mailItem->title = $data['subject'];
                        $mailItem->content = \View::make('emails.newDocument', $data);
                        $mailItem->save();

                        $log = new \App\MailLog();
                        $log->module_id = $module_id;
                        $log->user_id = $user->id;
                        $log->entity_id = $document->document;
                        $log->save();
                    }else{
                        \Log::info("User ".$user->name." is already notified about ".$document->document." for module ".$module_id);
                    }



                }



            }
        }
    }
}
