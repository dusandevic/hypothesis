<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MailItem extends Model
{
    public function mail()
    {
        return $this->belongsTo('App\Mail');
    }

    public function scopeUngrouped($query)
    {
        return $query->where('mail_id', null);
    }
}
